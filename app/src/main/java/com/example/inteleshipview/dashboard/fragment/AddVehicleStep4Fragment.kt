package com.example.inteleshipview.dashboard.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentAddVehicleStep4Binding

class AddVehicleStep4Fragment : BaseFragment() {

    private lateinit var binding: FragmentAddVehicleStep4Binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddVehicleStep4Binding.inflate(layoutInflater)

        binding.addBtn.setOnClickListener {
            checkValidation()
        }

        return binding.root
    }

    private fun checkValidation() {
        if (binding.deadlineEdt.text.isNullOrEmpty()) {
            //TODO in case of empty Edit text
        }
    }
}
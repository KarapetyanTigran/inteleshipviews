package com.example.inteleshipview.dashboard.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentAddVehicleStep1Binding

class AddVehicleStep1Fragment : BaseFragment() {

    private lateinit var binding: FragmentAddVehicleStep1Binding
    private val editTextList = mutableListOf<AppCompatEditText>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddVehicleStep1Binding.inflate(layoutInflater)

        binding.apply {
            editTextList.add(vinEdt)
            editTextList.add(makeEdt)
            editTextList.add(modelEdt)
            editTextList.add(yearEdt)
            editTextList.add(pickedUpPlaceEdt)
            editTextList.add(auctionEdt)
            editTextList.add(auctionSiteEdt)
        }

        binding.nextStepBtn.setOnClickListener {
            checkValidation()
        }
        binding.vinCodeDecodeBtn.setOnClickListener {
            //TODO in this case must open Camera
        }

        return binding.root
    }

    private fun checkValidation() {
        editTextList.forEach {
            if (it.text.isNullOrEmpty()) {
                //TODO in case of empty Edit text
            }
        }
    }
}
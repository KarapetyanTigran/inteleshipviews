package com.example.inteleshipview.dashboard.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentDeliveredInfoBinding

class DeliveredInfoFragment : BaseFragment() {


    private lateinit var binding: FragmentDeliveredInfoBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDeliveredInfoBinding.inflate(layoutInflater)
        return binding.root
    }
}
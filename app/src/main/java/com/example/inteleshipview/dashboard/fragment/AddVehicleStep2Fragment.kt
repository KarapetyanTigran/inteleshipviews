package com.example.inteleshipview.dashboard.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentAddVehicleStep2Binding

class AddVehicleStep2Fragment : BaseFragment() {

    private lateinit var binding: FragmentAddVehicleStep2Binding
    private val editTextList = mutableListOf<AppCompatEditText>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddVehicleStep2Binding.inflate(layoutInflater)

        binding.apply {
            editTextList.add(dealerNameEdt)
            editTextList.add(addressStreetEdt)
            editTextList.add(addressApartmentEdt)
            editTextList.add(zipCodeEdt)
            editTextList.add(cityCodeEdt)
            editTextList.add(stateEdt)
        }

        binding.nextBtn.setOnClickListener {
            checkValidation()
        }

        return binding.root
    }

    private fun checkValidation() {
        editTextList.forEach {
            if (it.text.isNullOrEmpty()) {
                //TODO in case of empty Edit text
            }
        }
    }
}
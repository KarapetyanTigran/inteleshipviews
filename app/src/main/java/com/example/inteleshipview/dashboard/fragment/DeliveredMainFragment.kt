package com.example.inteleshipview.dashboard.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentDeliveredMainBinding

class DeliveredMainFragment : BaseFragment() {

    private lateinit var binding: FragmentDeliveredMainBinding
    private val editTextList = mutableListOf<AppCompatEditText>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentDeliveredMainBinding.inflate(layoutInflater)

        binding.apply {
            editTextList.add(dealerEdt)
            editTextList.add(priceEdt)
            editTextList.add(requestedDateEdt)
            editTextList.add(estimatedDateEdt)
            editTextList.add(userNameEdt)
            editTextList.add(actualDeliveryDateEdt)
            editTextList.add(commentEdt)
        }

        binding.confirmBtn.setOnClickListener {
            checkValidation()
        }

        return binding.root
    }

    private fun checkValidation() {
        editTextList.forEach {
            if (it.text.isNullOrEmpty()) {
                //TODO in case of empty Edit text
            }
        }
    }
}
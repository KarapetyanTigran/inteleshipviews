package com.example.inteleshipview.util

import androidx.appcompat.widget.AppCompatEditText

const val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$"
const val EMAIL_PATTERN = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"

fun AppCompatEditText.checkPattern(pattern: String): Boolean {
    return this.text?.trim()?.matches(pattern.toRegex()) == true
}
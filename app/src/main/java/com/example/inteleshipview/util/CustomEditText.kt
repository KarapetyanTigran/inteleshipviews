package com.example.inteleshipview.util

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatEditText
import com.example.inteleshipview.R
import java.util.logging.Handler
import kotlin.math.absoluteValue


/*
fun AppCompatEditText.setErrorState(state: Boolean) {
this.context.obtainStyledAttributes(intArrayOf( R.styleable.editТext_state_error)).is
}*/

class CustomEditText(context: Context, attrs: AttributeSet?,defStyle:Int) :
    AppCompatEditText(context, attrs,defStyle) {
    private var isError = false

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.CustomEditText,
            0, 0
        ).apply {
            try {
                isError = getBoolean(R.styleable.CustomEditText_state_error, false)
            } finally {
                recycle()
            }
        }
    }

    override fun onCreateDrawableState(extraSpace: Int): IntArray {
        val state = super.onCreateDrawableState(extraSpace + 1)
        if (isError) {
            mergeDrawableStates(state, intArrayOf(R.attr.state_error))
        }
        return state
    }

    fun setIsError(isError: Boolean) {
        this.isError = isError
        refreshDrawableState()
    }

    fun isError(): Boolean {
        return isError
    }


}

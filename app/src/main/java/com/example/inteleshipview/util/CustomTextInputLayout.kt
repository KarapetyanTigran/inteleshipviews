package com.example.inteleshipview.util

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import com.example.inteleshipview.R
import com.google.android.material.textfield.TextInputLayout

class CustomTextInputLayout(context: Context, attrs: AttributeSet) :
    TextInputLayout(context, attrs) {
    private var errorTextGravity = Gravity.START

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.CustomTextInput,
            0, 0
        ).apply {
            try {
                errorTextGravity =
                    getInteger(R.styleable.CustomTextInput_error_text_gravity, Gravity.START)
            } finally {
                recycle()
            }
        }

    }

    override fun setErrorTextAppearance(errorTextAppearance: Int) {
        val errorTextView = this.findViewById<TextView>(com.google.android.material.R.id.textinput_error)
        val errorFrameLayout = errorTextView.parent as FrameLayout
        errorTextView.gravity = Gravity.END
        errorFrameLayout.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
    }
}
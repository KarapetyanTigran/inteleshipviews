package com.example.inteleshipview.util

import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.google.android.material.tabs.TabLayout.Tab

fun TabLayout.setTabs(mutableList: MutableList<Pair<String, Int>>) {
    mutableList.forEach {
        this.addTab(this.newTab().apply {
            id = it.second
            text = it.first
        })
    }
}

fun TabLayout.setTabs(vararg items: Pair<String, Int>) {
    items.forEach {
        this.addTab(this.newTab().apply {
            id = it.second
            text = it.first
        })
    }
}

fun TabLayout.setSelectedTabListener(listener: SelectedTabInterface) {
    this.addOnTabSelectedListener(listener)
}

fun interface SelectedTabInterface : OnTabSelectedListener {

    fun onTabItemSelected(tab: Tab?)

    override fun onTabUnselected(tab: Tab?) {}

    override fun onTabReselected(tab: Tab?) {}

    override fun onTabSelected(tab: Tab?) {
        onTabItemSelected(tab)
    }
}


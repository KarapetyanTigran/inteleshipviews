package com.example.inteleshipview.more.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentUserPersonalInformationBinding
import com.example.inteleshipview.util.EMAIL_PATTERN
import com.example.inteleshipview.util.checkPattern

class UserPersonalInformationFragment : BaseFragment() {

    private lateinit var binding: FragmentUserPersonalInformationBinding
    private val editTextList = mutableListOf<AppCompatEditText>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserPersonalInformationBinding.inflate(layoutInflater)

        binding.apply {
            editTextList.add(emailEdt)
            editTextList.add(phoneNumberEdt)
            editTextList.add(firstNameEdt)
            editTextList.add(lastNameEdt)
        }
        binding.saveBtn.setOnClickListener {
            checkValidation()
        }
        return binding.root
    }

    private fun checkValidation() {
        editTextList.forEach {
            if (it.text.isNullOrEmpty()) {
                //TODO in case of empty Edit text
            } else {
                if (!binding.emailEdt.checkPattern(EMAIL_PATTERN)) {
                    // TODO in case of inconsistency of the pattern
                }
            }
        }
    }
}
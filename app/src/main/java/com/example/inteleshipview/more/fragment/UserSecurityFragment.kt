package com.example.inteleshipview.more.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentUserSecurityBinding
import com.example.inteleshipview.util.PASSWORD_PATTERN
import com.example.inteleshipview.util.checkPattern

class UserSecurityFragment : BaseFragment() {

    private lateinit var binding: FragmentUserSecurityBinding
    private val editTextList = mutableListOf<AppCompatEditText>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserSecurityBinding.inflate(layoutInflater)

        binding.apply {
            editTextList.add(oldPasswordEdt)
            editTextList.add(newPasswordEdt)
            editTextList.add(confirmNewPasswordEdt)
        }

        binding.saveBtn.setOnClickListener {
            checkValidation()
        }

        return binding.root
    }

    private fun checkValidation() {
        editTextList.forEach {
            if (it.text.isNullOrEmpty()) {
                // TODO in case of empty Edit text
            } else {
                if (!binding.newPasswordEdt.checkPattern(PASSWORD_PATTERN) ||
                    !binding.confirmNewPasswordEdt.checkPattern(PASSWORD_PATTERN) ||
                    !checkMatchConfirmPassword()
                ) {
                    // TODO handle cases of invalid input or mismatched passwords
                }
            }
        }
    }

    private fun checkMatchConfirmPassword(): Boolean {
        return binding.newPasswordEdt.text.toString() == binding.confirmNewPasswordEdt.text.toString()
    }
}
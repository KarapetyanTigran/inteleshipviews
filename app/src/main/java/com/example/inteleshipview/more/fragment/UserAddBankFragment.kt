package com.example.inteleshipview.more.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentUserAddBankBinding

class UserAddBankFragment : BaseFragment() {

    private lateinit var binding: FragmentUserAddBankBinding
    private val editTextList = mutableListOf<AppCompatEditText>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserAddBankBinding.inflate(layoutInflater)

        binding.apply {
            editTextList.add(cardNumberEdt)
            editTextList.add(routingNumberEdt)
            editTextList.add(accountTypeEdt)
            editTextList.add(accountNumberEdt)
            editTextList.add(nameOnAccountEdt)
        }
        binding.addBankBtn.setOnClickListener {
            checkValidation()
        }

        return binding.root
    }

    private fun checkValidation() {
        editTextList.forEach {
            if (it.text.isNullOrEmpty()) {
                //TODO in case of empty Edit text
            }
        }
    }
}
package com.example.inteleshipview.more.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentUserResetBinding
import com.example.inteleshipview.util.PASSWORD_PATTERN
import com.example.inteleshipview.util.checkPattern

class UserResetFragment : BaseFragment() {

    private lateinit var binding: FragmentUserResetBinding
    private val editTextList = mutableListOf<AppCompatEditText>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserResetBinding.inflate(layoutInflater)
        binding.apply {
            editTextList.add(emailEdt)
            editTextList.add(passwordEdt)
            editTextList.add(confirmPasswordEdt)
        }

        binding.resetBtn.setOnClickListener {
            checkValidation()
        }

        return binding.root
    }

    private fun checkValidation() {
        editTextList.forEach {
            if (it.text.isNullOrEmpty()) {
                // TODO in case of empty Edit text
            } else {
                if (!binding.passwordEdt.checkPattern(PASSWORD_PATTERN) ||
                    !binding.confirmPasswordEdt.checkPattern(PASSWORD_PATTERN) ||
                    !checkMatchConfirmPassword()
                ) {
                    // TODO handle cases of invalid input or mismatched passwords
                }
            }
        }
    }

    private fun checkMatchConfirmPassword(): Boolean {
        return binding.passwordEdt.text.toString() == binding.confirmPasswordEdt.text.toString()
    }
}
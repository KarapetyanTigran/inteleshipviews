package com.example.inteleshipview.more.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentUserAddCardBinding

class UserAddCardsFragment : BaseFragment() {

    private lateinit var binding: FragmentUserAddCardBinding
    private val editTextList = mutableListOf<AppCompatEditText>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserAddCardBinding.inflate(layoutInflater)

        binding.apply {
            editTextList.add(cardNumberEdt)
            editTextList.add(nameOnCardEdt)
            editTextList.add(expirationDateEdt)
            editTextList.add(cvvCodeEdt)
            editTextList.add(addressEdt)
            editTextList.add(address2Edt)
            editTextList.add(zipCodeEdt)
            editTextList.add(cityEdt)
            editTextList.add(stateCodeEdt)
        }
        binding.addCardBtn.setOnClickListener {
            checkValidation()
        }

        return binding.root
    }

    private fun checkValidation() {
        editTextList.forEach {
            if (it.text.isNullOrEmpty()) {
                //TODO in case of empty Edit text
            }
        }
    }
}
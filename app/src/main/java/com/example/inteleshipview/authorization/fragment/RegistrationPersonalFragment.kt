package com.example.inteleshipview.authorization.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentRegistrationPersonalBinding

class RegistrationPersonalFragment : BaseFragment() {

    private lateinit var binding: FragmentRegistrationPersonalBinding
    private val editTextList = mutableListOf<AppCompatEditText>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegistrationPersonalBinding.inflate(layoutInflater)

        binding.apply {
            editTextList.add(firstNameEdt)
            editTextList.add(lastNameEdt)
            editTextList.add(emailAddressEdt)
            editTextList.add(phoneNumberEdt)
        }

        binding.completeBtn.setOnClickListener {
            checkValidation()
        }

        return binding.root
    }

    private fun checkValidation() {
        editTextList.forEach {
            if (it.text.isNullOrEmpty()) {
                // TODO in case of empty Edit text
            }
        }
    }
}
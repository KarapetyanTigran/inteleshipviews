package com.example.inteleshipview.authorization.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentRegistrationCompanyBinding

class RegistrationCompanyFragment : BaseFragment() {

    private lateinit var binding: FragmentRegistrationCompanyBinding
    private val editTextList = mutableListOf<AppCompatEditText>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegistrationCompanyBinding.inflate(layoutInflater)

        binding.apply {
            editTextList.add(legalNameEdt)
            editTextList.add(companyNameEdt)
            editTextList.add(addressStreetEdt)
            editTextList.add(addressApartmentEdt)
            editTextList.add(zipCodeEdt)
            editTextList.add(cityCodeEdt)
            editTextList.add(stateEdt)
        }

        binding.nextStepBtn.setOnClickListener {
            checkValidation()
        }

        return binding.root
    }

    private fun checkValidation() {
        editTextList.forEach {
            if (it.text.isNullOrEmpty()) {
                // TODO in case of empty Edit text
            }
        }
    }
}
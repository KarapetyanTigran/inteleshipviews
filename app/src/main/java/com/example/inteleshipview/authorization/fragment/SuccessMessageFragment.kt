package com.example.inteleshipview.authorization.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentSuccessBinding

class SuccessMessageFragment : BaseFragment() {

    private lateinit var binding: FragmentSuccessBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentSuccessBinding.inflate(layoutInflater)
        return binding.root
    }
}
package com.example.inteleshipview.authorization.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentLoginBinding
import com.example.inteleshipview.util.EMAIL_PATTERN
import com.example.inteleshipview.util.PASSWORD_PATTERN
import com.example.inteleshipview.util.checkPattern

class LoginFragment : BaseFragment() {

    lateinit var binding: FragmentLoginBinding
    private val editTextList = mutableListOf<AppCompatEditText>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(layoutInflater)

        editTextList.add(binding.emailEdt)
        editTextList.add(binding.passwordEdt)

        binding.apply {
            loginBtn.setOnClickListener {
                checkValidation()
            }
        }
        return binding.root
    }

    private fun checkValidation() {
        editTextList.forEach {
            if (it.text.isNullOrEmpty()) {
                // TODO in case of empty Edit text
            } else {
                when (it) {
                    binding.emailEdt -> it.checkPattern(EMAIL_PATTERN)
                    // TODO in case of inconsistency of the pattern
                    else -> it.checkPattern(PASSWORD_PATTERN)
                    // TODO in case of inconsistency of the pattern
                }
            }
        }
    }
}
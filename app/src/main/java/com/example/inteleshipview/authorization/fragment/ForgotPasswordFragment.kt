package com.example.inteleshipview.authorization.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentForgotPasswordBinding
import com.example.inteleshipview.util.EMAIL_PATTERN
import com.example.inteleshipview.util.checkPattern

class ForgotPasswordFragment : BaseFragment() {

    private lateinit var binding: FragmentForgotPasswordBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentForgotPasswordBinding.inflate(layoutInflater)

        binding.apply {
            gotItBtn.setOnClickListener {
                if (binding.emailEdt.text.isNullOrEmpty()) {
                    // TODO in case of empty Edit text
                } else if (!binding.emailEdt.checkPattern(EMAIL_PATTERN)) {
                    // TODO in case of inconsistency of the pattern
                }
            }
        }
        return binding.root
    }
}
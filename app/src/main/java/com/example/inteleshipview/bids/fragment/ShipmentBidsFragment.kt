package com.example.inteleshipview.bids.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.inteleshipview.base.BaseFragment
import com.example.inteleshipview.databinding.FragmentShipmentBidsBinding


class ShipmentBidsFragment : BaseFragment() {
    private lateinit var binding: FragmentShipmentBidsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShipmentBidsBinding.inflate(this.layoutInflater)

        return binding.root
    }
}
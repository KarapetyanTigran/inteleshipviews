package com.example.inteleshipview.main.data

import android.os.Parcelable
import androidx.fragment.app.Fragment
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class ViewPagerDataModel(
    var fragmentList: MutableList<FragmentModel> = mutableListOf(),
    var isFilterLayoutVisible: Boolean = false,
    var isFilterItemVisible: Boolean = true
) : Parcelable

@Parcelize

data class FragmentModel(
    var fragment: @RawValue Fragment,
    val title: String,
) : Parcelable


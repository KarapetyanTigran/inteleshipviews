package com.example.inteleshipview.main.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.inteleshipview.authorization.fragment.LoginFragment
import com.example.inteleshipview.main.fragment.TabLayoutFragment
import com.example.inteleshipview.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportFragmentManager
            .beginTransaction()
            .replace(binding.fragmentContainer.id, LoginFragment())
            .commit()
    }
}
package com.example.inteleshipview.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.inteleshipview.dashboard.fragment.DeliveredInfoFragment
import com.example.inteleshipview.dashboard.fragment.DeliveredMainFragment
import com.example.inteleshipview.databinding.FragmentMainTabLayoutBinding
import com.example.inteleshipview.main.adapter.ViewPagerAdapter
import com.example.inteleshipview.main.data.FragmentModel
import com.example.inteleshipview.main.data.ViewPagerDataModel
import com.google.android.material.tabs.TabLayoutMediator

class TabLayoutFragment : Fragment() {

    private lateinit var binding: FragmentMainTabLayoutBinding
    private var viewPagerDataModel: ViewPagerDataModel = ViewPagerDataModel()
    private var positionList = mutableListOf<Int>()
    private var defaultPosition = 0
    private lateinit var callback: OnBackPressedCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                handleBackPressed()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainTabLayoutBinding.inflate(layoutInflater)
        viewPagerDataModel = arguments?.getParcelable(EXTRA_FRAGMENT_LIST) ?: ViewPagerDataModel()
        viewPagerDataModel.fragmentList.add(FragmentModel(DeliveredInfoFragment(), "DeliveredInfo"))
        viewPagerDataModel.fragmentList.add(FragmentModel(DeliveredMainFragment(), "Main"))
        viewPagerDataModel.fragmentList.add(FragmentModel(DeliveredInfoFragment(), "DeliveredInfo"))
        viewPagerDataModel.fragmentList.add(FragmentModel(DeliveredMainFragment(), "Main"))
        val viewPagerAdapter: ViewPagerAdapter by lazy { ViewPagerAdapter(this, viewPagerDataModel.fragmentList) }
        binding.viewPager.apply {
            adapter = viewPagerAdapter
            isUserInputEnabled = false
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    positionList.remove(position)
                    positionList.add(position)
                }
            })
        }
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = viewPagerAdapter.fragmentList[position].title
        }.attach()
        requireActivity().onBackPressedDispatcher.addCallback(callback)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        callback.isEnabled = true
    }

    override fun onPause() {
        super.onPause()
        callback.isEnabled = false
    }

    private fun handleBackPressed() {
        positionList.removeLastOrNull()
        callback.isEnabled =
            positionList.isNotEmpty() || binding.viewPager.currentItem != defaultPosition
        if (callback.isEnabled) binding.viewPager.currentItem =
            positionList.lastOrNull() ?: defaultPosition
        else requireActivity().onBackPressed()
    }

    companion object {

        const val EXTRA_FRAGMENT_LIST = "fragmentListExtra"

        fun newInstance(viewPagerDataModel: ViewPagerDataModel) =
            TabLayoutFragment().apply {
                arguments = bundleOf(EXTRA_FRAGMENT_LIST to viewPagerDataModel)
            }
    }
}
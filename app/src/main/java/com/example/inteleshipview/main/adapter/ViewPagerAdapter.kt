package com.example.inteleshipview.main.adapter


import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.inteleshipview.main.data.FragmentModel
import com.example.inteleshipview.main.data.ViewPagerDataModel

class ViewPagerAdapter(
    private val fragment: Fragment,
    val fragmentList: MutableList<FragmentModel>
) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return fragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
        /*     fragment.parentFragmentManager.beginTransaction()
                 .addToBackStack(fragmentList[position].fragment.id.toString()
                 ).commit()*/
        return fragmentList[position].fragment
    }
}